using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RichText
{
    /// <summary>
    /// Returns a text inside a color tag to use in text components in unity.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string GetColoredText(string text, Color color) => $"<color=#{color.ToHex()}>{text}</color>";
}