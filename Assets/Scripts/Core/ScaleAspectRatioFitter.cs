﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class ScaleAspectRatioFitter : MonoBehaviour
{
    [SerializeField] private float parentFactorToScale = 1f;
    [SerializeField] private bool checkWidth = true;
    [SerializeField] private bool checkHeight = false;
    private RectTransform parent;
    private RectTransform rectTransform;


    private void SetRectTransforms()
    {
        if (transform.parent == null)
        {
            Debug.LogError($"Your {name} instance needs a parent with a rect transform for {typeof(ScaleAspectRatioFitter)} to work.");
            return;
        }
        parent = (RectTransform)transform.parent;
        rectTransform = (RectTransform)transform;
    }

    private void Start()
    {
        SetRectTransforms();
        UpdateRectScale();
    }

    private void Update()
    {
        UpdateRectScale();
    }

    private void UpdateRectScale()
    {
        if (parent == null || rectTransform == null)
        {
            SetRectTransforms();
            Debug.LogWarning($"Make sure that the {name} instance has a rect transform component so {typeof(ScaleAspectRatioFitter)} can work.");
            return;
        }
        if (checkWidth)
            CheckWidth();
        if (checkHeight)
            CheckHeight();
    }

    private void CheckWidth()
    {
        float parentWidth = parent.rect.width * parentFactorToScale;
        if (rectTransform.rect.width > parentWidth)
        {
            float scaleFactor = parentWidth / rectTransform.rect.width;
            transform.localScale = Vector3.one * scaleFactor;
        }
        else
            transform.localScale = Vector3.one;
    }

    private void CheckHeight()
    {
        if (rectTransform.rect.height > parent.rect.height)
        {
            float scaleFactor = parent.rect.height / rectTransform.rect.height;
            transform.localScale = Vector3.one * scaleFactor;
        }
        else
            transform.localScale = Vector3.one;
    }
}
