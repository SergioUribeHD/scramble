using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] private float startingTime = 120f;

    public event Action<float> OnTimeUpdated = null;
    public event Action OnTimeUp = null;

    public float CurrentTimeNormalized => currentTime / startingTime;

    private float currentTime;

    public bool Enabled { get; set; } = true;
    public bool Paused { get; set; } = false;

    private void Start()
    {
        Restart();
    }

    private void Update()
    {
        if (currentTime == 0 || !Enabled || Paused) return;
        currentTime = Mathf.Max(currentTime - Time.deltaTime, 0f);
        OnTimeUpdated?.Invoke(CurrentTimeNormalized);
        if (currentTime > 0) return;
        OnTimeUp?.Invoke();
    }

    public void Restart()
    {
        Paused = false;
        currentTime = startingTime;
    }
}
