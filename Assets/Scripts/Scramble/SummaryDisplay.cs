using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryDisplay : MonoBehaviour
{
    [SerializeField] private GameObject panel = null;
    [SerializeField] private ScrambleStageHandler scrambleHandler = null;
    [SerializeField] private ScoreHandler scoreHandler = null;
    [SerializeField] private Text statsText = null;
    [SerializeField] private VarReplacer statsReplacer = new VarReplacer("Respuestas correctas: {ss}/{ts}\nPuntos respuestas: {gs}\nBonus tiempo: {bt}");
    [SerializeField] private Color bonusColor = Color.green;
    [SerializeField] private Text totalScoreText = null;
    [SerializeField] private VarReplacer totalScoreReplacer = new VarReplacer("Total: {s} pts");


    private void Awake()
    {
        scrambleHandler.OnStageFinished += HandleStageFinished;
    }

    private void Start()
    {
        panel.SetActive(false);
    }

    private void OnDestroy()
    {
        scrambleHandler.OnStageFinished -= HandleStageFinished;
    }

    private void HandleStageFinished()
    {
        panel.SetActive(true);
        string totalTime = scoreHandler.TotalTimeBonus > 0 ? bonusColor.ToRichText($"+{scoreHandler.TotalTimeBonus}") : "0";
        string remainingAttempts = scoreHandler.RemainingAttemptsBonus > 0 ? bonusColor.ToRichText($"+{scoreHandler.RemainingAttemptsBonus}") : "0";
        var stats = new object[]
        {
            scrambleHandler.Streak,
            scrambleHandler.TotalProgressCount,
            scoreHandler.GrossScore,
            totalTime,
            remainingAttempts
        };
        statsText.text = statsReplacer.GetReplacedText(stats);
        totalScoreText.text = totalScoreReplacer.GetReplacedText(scoreHandler.TotalScore);
    }
}
