using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHandler : MonoBehaviour
{
    [SerializeField] private int scorePerRightGuess = 100;
    [SerializeField] private int bonusPerRemainingAttempt = 50;

    public event Action OnUpdated = null;
    public event Action<int> OnRecordUpdated = null;

    private static int recordScore;


    public int CurrentTimeBonus { get; private set; }

    public int GrossScore { get; private set; }
    public int TotalTimeBonus { get; private set; }
    public int RemainingAttemptsBonus { get; private set; }

    public int TotalScore => GrossScore + TotalTimeBonus + RemainingAttemptsBonus;

    private void Start()
    {
        OnRecordUpdated?.Invoke(recordScore);
    }

    public void AddScore()
    {
        GrossScore += scorePerRightGuess;
        TotalTimeBonus += CurrentTimeBonus;
        OnUpdated?.Invoke();
    }
    public void CheckRecordBroken()
    {
        if (GrossScore <= recordScore) return;
        recordScore = GrossScore;
        OnRecordUpdated?.Invoke(recordScore);
    }

    public void UpdateCurrentBonus(float bonusFactor)
    {
        CurrentTimeBonus = Mathf.RoundToInt(scorePerRightGuess * bonusFactor);
    }

    public void AddBonusPerRemainingAttempt(int attemptsCount)
    {
        RemainingAttemptsBonus += bonusPerRemainingAttempt * attemptsCount;
    }
}

