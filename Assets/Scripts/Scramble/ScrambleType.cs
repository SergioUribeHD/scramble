using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScrambleType
{
    None,
    Letter,
    Sentence
}
