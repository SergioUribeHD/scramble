using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SentenceSummaryDisplay : MonoBehaviour
{
    [SerializeField] private GameObject panel = null;
    [SerializeField] private SentenceStageHandler handler = null;
    [SerializeField] private Text streakText = null;
    [SerializeField] private VarReplacer streakReplacer = null;

    private void Awake()
    {
        handler.OnStageFinished += HandleStageFinished;
    }

    private void Start()
    {
        panel.SetActive(false);
    }

    private void OnDestroy()
    {
        handler.OnStageFinished -= HandleStageFinished;
    }

    private void HandleStageFinished(int streak, int sentenceCount)
    {
        panel.SetActive(true);
        streakText.text = streakReplacer.GetReplacedText(new object[] { streak, sentenceCount });
    }
}
