using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SentenceProgressDisplay : MonoBehaviour
{
    [SerializeField] private SentenceHandler handler = null;
    [SerializeField] private Text text = null;
    [SerializeField] private VarReplacer replacer = null;

    private void Awake()
    {
        handler.OnSentencePicked += HandleSentencePicked;
    }

    private void OnDestroy()
    {
        handler.OnSentencePicked -= HandleSentencePicked;
    }

    private void HandleSentencePicked(int initialCount, int currentCount)
    {
        text.text = replacer.GetReplacedText(new object[] { initialCount, currentCount });
    }
}
