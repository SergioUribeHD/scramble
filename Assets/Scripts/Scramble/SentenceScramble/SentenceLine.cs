using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SentenceLine
{
    [field: SerializeField] public string[] SentenceLines { get; private set; } = new string[3] { string.Empty, string.Empty, string.Empty };

    private string sentence;

    public string Sentence
    {
        get
        {
            if (!string.IsNullOrEmpty(sentence)) return sentence;
            sentence = SentenceLines[0];
            for (int i = 1; i < SentenceLines.Length; i++)
                sentence += $" {SentenceLines[i]}";
            return sentence;
        }
    }
}
