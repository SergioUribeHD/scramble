using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SentenceLineRack : MonoBehaviour
{
    [SerializeField] private List<LetterContainer> containers = null;
    public static event Action<Transform> OnOverlappingWord = null;

    public string Value
    {
        get
        {
            string value = containers[0].Value;
            for (int i = 1; i < containers.Count; i++)
            {
                if (!containers[i - 1].Value.Equals(""))
                {
                    if (!containers[i].Value.Equals(""))
                        value += $"{containers[i].Value}";
                    else if (i < containers.Count - 1)
                    {
                        if (containers[i + 1].Value.Equals(""))
                            break;
                        else
                            value += " ";
                    }

                }
                else
                    value += $"{containers[i].Value}";

            }
            return value;
        }
    }

    public bool MissesFirstWord => containers[0].transform.childCount == 0;
    
    public bool TrySetWord(List<Letter> letters)
    {
        bool space = false;
        int j = 0;
        int startingI = 0;
        for (int i = 0; i < containers.Count; i++)
        {
            if (containers[i].transform.childCount == 0 && i == 0) break;
            if (containers[i].transform.childCount == 0 && i > 0 && space) break;
            if (containers[i].transform.childCount > 0)
            {
                startingI++;
                space = false;
            }
            if (containers[i].transform.childCount == 0 && i > 0 && !space)
            {
                startingI++;
                space = true;
            }

        }
        for (int i = startingI; i < containers.Count; i++)
        {
            if (j == letters.Count) break;
            //if (j == 0) Debug.Log($"i + lettersCount = {i + letters.Count}. containersCount: {containers.Count}");
            if (i + letters.Count > containers.Count && j == 0) return false;
            letters[j].Container = containers[i];
            if (containers[i].transform.childCount > 1)
                OnOverlappingWord?.Invoke(containers[i].transform.GetChild(0));
            j++;
        }
        return startingI < containers.Count;
    }
}
