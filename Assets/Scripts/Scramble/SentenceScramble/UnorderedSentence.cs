using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnorderedSentence : MonoBehaviour
{
    [SerializeField] private float maxWidth = 3156f;
    [SerializeField] private List<UnorderedLineRack> lines = new List<UnorderedLineRack>();
    [SerializeField] private WordRack wordRackPrefab = null;

    public bool Empty => lines.FindAll(l => l.Empty).Count == lines.Count;

    public void Set(SentenceLine sentence)
    {
        StartCoroutine(CallSet(sentence));
    }

    private IEnumerator CallSet(SentenceLine sentence)
    {
        foreach (var line in lines)
            line.Clear();
        var words = new List<string>();
        foreach (var sentenceLine in sentence.SentenceLines)
            words.AddRange(sentenceLine.Split(' '));
        while (words.Count > 0)
        {
            var word = Instantiate(wordRackPrefab, transform);
            word.Set(words.GetRandomItem(true));
            foreach (var line in lines)
                if (line.TrySetWord(word, maxWidth)) break;
        }
        yield return new WaitForSeconds(0.3f);
        foreach (var line in lines)
            line.DisableLayout();

    }

    public bool TryGetWord(Transform letterTransform, out WordRack word)
    {
        word = null;
        foreach (var line in lines)
        {
            if (line.TryGetWord(letterTransform, out word)) return true;
        }
        return false;
    }

    public void Restart()
    {
        foreach (var line in lines)
        {
            line.Restart();
        }
    }
}
