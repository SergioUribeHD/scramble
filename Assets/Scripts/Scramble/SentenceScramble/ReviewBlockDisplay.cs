using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviewBlockDisplay : MonoBehaviour
{
    [SerializeField] private Image image = null;
    [SerializeField] private Sprite correctSpr = null;
    [SerializeField] private Sprite wrongSpr = null;
    [SerializeField] private Text text = null;

    public void Set(bool correct, string sentence)
    {
        gameObject.SetActive(true);
        image.sprite = correct ? correctSpr : wrongSpr;
        text.text = sentence;
    }
}
