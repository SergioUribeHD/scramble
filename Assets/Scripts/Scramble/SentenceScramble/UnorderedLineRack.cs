using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnorderedLineRack : MonoBehaviour
{
    [SerializeField] private HorizontalLayoutGroup layout = null;
    [SerializeField] private ContentSizeFitter fitter = null;

    private List<WordRack> words = new List<WordRack>();

    public bool Empty => words.FindAll(w => w.transform.childCount == 0).Count == words.Count;

    public void Clear()
    {
        foreach (var word in words)
            Destroy(word.gameObject);
        words.Clear();
        layout.enabled = fitter.enabled = true;
    }

    public bool TrySetWord(WordRack word, float maxWidth)
    {
        //Debug.Log(word.Size);
        if (GetTotalWidth(word) > maxWidth) return false;
        word.transform.SetParent(transform);
        word.transform.localScale = Vector3.one;
        words.Add(word);
        layout.spacing = word.LetterSize;
        return true;
    }

    private float GetTotalWidth(WordRack newWord)
    {
        if (words.Count == 0) return 0;
        float width = newWord.Size;
        width += words[0].LetterSize * (words.Count - 1);
        foreach (var word in words)
            width += word.Size;
        return width;
    }

    public void DisableLayout() =>  StartCoroutine(CallLayoutElementDisable());
    private IEnumerator CallLayoutElementDisable()
    {
        yield return new WaitForEndOfFrame();
        layout.enabled = fitter.enabled = false;
    }

    public bool TryGetWord(Transform letterTransform, out WordRack word)
    {
        word = null;
        foreach (var w in words)
        {
            if (w.Contains(letterTransform))
            {
                word = w;
                return true;
            }
        }
        return false;
    }

    public void Restart()
    {
        foreach (var word in words)
        {
            word.Restart();
        }
    }
}
