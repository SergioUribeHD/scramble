using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SentenceStageHandler : MonoBehaviour
{
    [SerializeField] private SentenceHandler handler = null;
    [SerializeField] private Timer timer = null;
    [SerializeField] private ScoreHandler scoreHandler = null;

    public event Action<int, int> OnStageFinished = null;

    private int sentencesCount;
    private int streak;

    private void Awake()
    {
        handler.OnSentencePicked += HandleSentencePicked;
        timer.OnTimeUp += HandleTimeUp;
        handler.OnSentenceSubmitted += HandleSentenceSubmitted;
        handler.OnSentencesCompleted += HandleSentencesCompleted;
    }

    private void OnDestroy()
    {
        handler.OnSentencePicked -= HandleSentencePicked;
        timer.OnTimeUp -= HandleTimeUp;
        handler.OnSentenceSubmitted -= HandleSentenceSubmitted;
        handler.OnSentencesCompleted -= HandleSentencesCompleted;
    }

    private void HandleSentencesCompleted()
    {
        OnStageFinished?.Invoke(streak, sentencesCount);
    }

    private void HandleSentencePicked(int initialCount, int currentCount)
    {
        sentencesCount = initialCount;
        timer.Restart();
    }

    private void HandleTimeUp()
    {
        handler.SubmitSentence();
    }

    private void HandleSentenceSubmitted(bool correct)
    {
        timer.Paused = true;
        if (!correct) return;
        streak++;
        scoreHandler.AddScore();
    }    
}
