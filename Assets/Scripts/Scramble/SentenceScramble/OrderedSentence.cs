using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderedSentence : MonoBehaviour
{
    [SerializeField] private List<SentenceLineRack> sentenceLines = null;

    public event Action OnSentenceUpdated = null;

    public string Sentence
    {
        get
        {
            string sentence = sentenceLines[0].Value;
            for (int i = 1; i < sentenceLines.Count; i++)
                sentence += $"{(sentenceLines[i - 1].Value.EndsWith(" ") ? string.Empty : " ")}{sentenceLines[i].Value}";
            return sentence;
        }
    }

    private int lastSentenceLineIndex;

    private void OnEnable()
    {
        WordRack.OnClick += HandleWordClicked;
    }

    private void OnDisable()
    {
        WordRack.OnClick -= HandleWordClicked;
    }

    private void SetWord(List<Letter> letters)
    {
        for (int i = lastSentenceLineIndex; i < sentenceLines.Count; i++)
        {
            if (sentenceLines[i].TrySetWord(letters))
            {
                lastSentenceLineIndex = i;
                break;
            }
        }
    }

    public void Restart() => lastSentenceLineIndex = 0;

    private void HandleWordClicked(List<Letter> letters)
    {
        if (letters[0].Container == null)
            SetWord(letters);
        else
        {
            foreach (var l in letters)
                l.Restart();
            for (int i = 0; i < sentenceLines.Count; i++)
            {
                if (sentenceLines[i].MissesFirstWord)
                {
                    lastSentenceLineIndex = i;
                    break;
                }
            }
        }
        OnSentenceUpdated?.Invoke();
    }

}
