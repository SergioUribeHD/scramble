using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SentenceHandler : MonoBehaviour
{
    [SerializeField] private UnorderedSentence unorderedSentence = null;
    [SerializeField] private OrderedSentence orderedSentence = null;
    [SerializeField] private Button restartBtn = null;
    [SerializeField] private Button submitBtn = null;
    [SerializeField] private Button nextBtn = null;
    [SerializeField] private GameObject blockPanel = null;
    [SerializeField] private ReviewBlockDisplay reviewBlock = null;

    [SerializeField] private SentenceLine[] sentenceLines = new SentenceLine[1];

    public event Action<int, int> OnSentencePicked = null;
    public event Action<bool> OnSentenceSubmitted = null;
    public event Action OnSentencesCompleted = null;

    private List<SentenceLine> sentencesInStage = new List<SentenceLine>();
    private SentenceLine currentSentence;

    private int initialSentenceCount;
    public bool CanPickSentence => sentencesInStage.Count > 0;

    private void OnValidate()
    {
        foreach (var sentenceLine in sentenceLines)
        {
            foreach (var line in sentenceLine.SentenceLines)
            {
                if (line.Length > 20)
                    Debug.LogError("One of your sentences has more than 20 characters.");
            }
        }
    }

    private void OnEnable()
    {
        restartBtn.gameObject.SetActive(true);
        submitBtn.onClick.AddListener(SubmitSentence);
        SentenceLineRack.OnOverlappingWord += HandleOverlappingWord;
        orderedSentence.OnSentenceUpdated += HandleWordClicked;
    }
    private void OnDisable()
    {
        restartBtn.gameObject.SetActive(false);
        submitBtn.onClick.RemoveListener(SubmitSentence);
        SentenceLineRack.OnOverlappingWord -= HandleOverlappingWord;
        orderedSentence.OnSentenceUpdated -= HandleWordClicked;
    }

    public int FillUpList()
    {
        sentencesInStage.AddRange(sentenceLines);
        initialSentenceCount = sentencesInStage.Count;
        return initialSentenceCount;
    }

    public bool TryUpdateSentence()
    {
        if (CanPickSentence)
        {
            PickSentence();
            return true;
        }
        OnSentencesCompleted?.Invoke();
        return false;
    }

    public void PickSentence()
    {
        gameObject.SetActive(true);
        orderedSentence.Restart();
        blockPanel.SetActive(false);
        reviewBlock.gameObject.SetActive(false);
        currentSentence = sentencesInStage.GetRandomItem(true);
        unorderedSentence.Set(currentSentence);
        submitBtn.gameObject.SetActive(true);
        nextBtn.gameObject.SetActive(false);
        submitBtn.interactable = unorderedSentence.Empty;
        OnSentencePicked?.Invoke(initialSentenceCount - sentencesInStage.Count, initialSentenceCount);
    }


    private void HandleOverlappingWord(Transform letterTransform)
    {
        if (!unorderedSentence.TryGetWord(letterTransform, out var word)) return;
        word.Restart();
    }

    private void HandleWordClicked()
    {
        submitBtn.interactable = unorderedSentence.Empty;
    }

    public void SubmitSentence()
    {
        bool correct = orderedSentence.Sentence.Equals(currentSentence.Sentence);
        //Debug.Log($"Ordered sentence: {orderedSentence.Sentence}\nCurrent: {currentSentence.Sentence}");
        blockPanel.SetActive(true);
        reviewBlock.Set(correct, currentSentence.Sentence);
        submitBtn.gameObject.SetActive(false);
        nextBtn.gameObject.SetActive(true);
        OnSentenceSubmitted?.Invoke(correct);
    }

    public void Restart()
    {
        orderedSentence.Restart();
        unorderedSentence.Restart();
    }

}
