using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordRack : MonoBehaviour
{
    [SerializeField] private Letter letterPrefab = null;
    [SerializeField] private HorizontalLayoutGroup layout = null;

    public float LetterSize => ((RectTransform)letters[0].transform).rect.width;  
    public float Size => (letters.Count * LetterSize) + ((letters.Count - 1) * layout.spacing);  

    private List<Letter> letters = new List<Letter>();

    public static event Action<List<Letter>> OnClick = null;

    //private void OnValidate()
    //{
    //    if (letterPrefab == null) return;
    //}

    private void OnEnable()
    {
        Letter.OnClick += HandleLetterClick;
    }

    private void OnDisable()
    {
        Letter.OnClick -= HandleLetterClick;
    }

    private void OnDestroy()
    {
        foreach (var letter in letters)
            Destroy(letter.gameObject);
    }

    public void Set(string word)
    {
        letters.CheckInstancesNeeded(letterPrefab, transform, word.Length);
        for (int i = 0; i < word.Length; i++)
            letters[i].Set(word[i], transform);
        for (int i = word.Length; i<letters.Count; i++)
            letters[i].SetActive(transform, false);
    }

    private void HandleLetterClick(Letter letter)
    {
        if (!letters.Contains(letter)) return;
        OnClick?.Invoke(letters);
        //Debug.Log(LetterSize);
        //Debug.Log(Size);
    }

    public bool Contains(Transform letterTransform)
    {
        if (!letterTransform.TryGetComponent<Letter>(out var letter)) return false;
        return letters.Contains(letter);
    }


    public void Restart()
    {
        foreach (var letter in letters)
            letter.Restart();
    }
}
