using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressHandler : MonoBehaviour
{
    [SerializeField] private SentenceHandler sentenceHandler = null;
    [SerializeField] private RequiredWordPicker wordPicker = null;

    public event Action OnProgressUpdated = null;

    public int CurrentCount => currentSentenceCount + currentWordCount;

    public int TotalCount
    {
        get => totalCount;
        set
        {
            totalCount = value;
            OnProgressUpdated?.Invoke();
        }
    }

    private int totalCount;

    private int currentSentenceCount;
    private int currentWordCount;

    private void Awake()
    {
        sentenceHandler.OnSentencePicked += HandleSentenceUpdated;
        wordPicker.OnWordPicked += HandleWordPicked;
    }

    private void OnDestroy()
    {
        sentenceHandler.OnSentencePicked -= HandleSentenceUpdated;
        wordPicker.OnWordPicked -= HandleWordPicked;
    }

    private void HandleSentenceUpdated(int currentCount, int initialCount)
    {
        currentSentenceCount = currentCount;
        OnProgressUpdated?.Invoke();
    }
    private void HandleWordPicked(int currentCount, int initialCount)
    {
        currentWordCount = currentCount;
        OnProgressUpdated?.Invoke();
    }


}
