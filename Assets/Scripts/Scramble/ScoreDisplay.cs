using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private ScoreHandler scoreHandler = null;
    [SerializeField] private Text text = null;

    private void Awake()
    {
        scoreHandler.OnUpdated += HandleScoreUpdated;
    }

    private void OnDestroy()
    {
        scoreHandler.OnUpdated -= HandleScoreUpdated;
    }

    private void Start()
    {
        text.text = scoreHandler.GrossScore.ToString();
    }

    private void HandleScoreUpdated()
    {
        text.text = scoreHandler.GrossScore.ToString();
    }
}

