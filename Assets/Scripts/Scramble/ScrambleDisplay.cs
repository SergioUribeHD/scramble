using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrambleDisplay : MonoBehaviour
{
    [SerializeField] private RequiredWordPicker wordPicker = null;
    [SerializeField] private SentenceHandler sentenceHandler = null;
    [SerializeField] private GameObject letterScramble = null;
    [SerializeField] private GameObject sentenceScramble = null;

    private void Awake()
    {
        wordPicker.OnWordPicked += HandleWordPicked;
        sentenceHandler.OnSentencePicked += HandleSentencePicked;
    }

    private void OnDestroy()
    {
        wordPicker.OnWordPicked -= HandleWordPicked;
        sentenceHandler.OnSentencePicked -= HandleSentencePicked;
    }

    private void HandleWordPicked(int initialCount, int currentCount)
    {
        letterScramble.SetActive(true);
        sentenceScramble.SetActive(false);
    }

    private void HandleSentencePicked(int initialCount, int currentCount)
    {
        letterScramble.SetActive(false);
        sentenceScramble.SetActive(true);
    }
}
