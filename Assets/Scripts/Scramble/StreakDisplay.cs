using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreakDisplay : MonoBehaviour
{
    [SerializeField] private ScrambleStageHandler scrambleHandler = null;
    [SerializeField] private ProgressHandler handler = null;
    [SerializeField] private ScoreHandler score = null;
    [SerializeField] private Text text = null;
    [SerializeField] private VarReplacer replacer = new VarReplacer("{s}/{t}");

    private void Awake()
    {
        score.OnUpdated += HandleScoreUpdated;
        HandleScoreUpdated();
    }

    private void OnDestroy()
    {
        score.OnUpdated -= HandleScoreUpdated;
    }

    private void HandleScoreUpdated()
    {
        text.text = replacer.GetReplacedText(new object[] { scrambleHandler.Streak, handler.TotalCount });
    }
}
