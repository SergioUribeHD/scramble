using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplay : MonoBehaviour
{
    [SerializeField] private Timer timer = null;
    [SerializeField] private Image fillArea = null;

    private void Awake()
    {
        timer.OnTimeUpdated += HandleTimeUpdated;
    }

    private void OnDestroy()
    {
        timer.OnTimeUpdated -= HandleTimeUpdated;
    }

    private void HandleTimeUpdated(float current)
    {
        fillArea.fillAmount = current;
    }
}
