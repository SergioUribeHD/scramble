using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseDisplay : MonoBehaviour
{
    [SerializeField] private ScrambleStageHandler handler = null;
    [SerializeField] private Live live = null;
    [SerializeField] private Image winLoseImg = null;
    [SerializeField] private Sprite winSpr = null;
    [SerializeField] private Sprite loseSpr = null;

    private void Awake()
    {
        handler.OnStageFinished += HandleStageFinished;
    }

    private void HandleStageFinished()
    {
        winLoseImg.sprite = live.LivesCount > 0 ? winSpr : loseSpr;
    }
}
