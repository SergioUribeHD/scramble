using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

public class ScrambleStageHandler : MonoBehaviour
{
    [SerializeField] private Timer timer = null;
    [SerializeField] private Live live = null;
    [SerializeField] private ScoreHandler scoreHandler = null;
    [SerializeField] private WordHandler wordHandler = null;
    [SerializeField] private RequiredWordPicker wordPicker = null;
    [SerializeField] private SentenceHandler sentenceHandler = null;
    [SerializeField] private ProgressHandler progressHandler = null;

    public event Action OnStageFinished = null;

    public int Streak { get; private set; }
    public int TotalProgressCount => progressHandler.TotalCount;

    private ScrambleType current;


    private void Awake()
    {
        wordHandler.OnWordSubmitted += HandleExerciseSubmitted;
        wordPicker.OnWordPicked += HandlePicked;
        sentenceHandler.OnSentenceSubmitted += HandleExerciseSubmitted;
        sentenceHandler.OnSentencePicked += HandlePicked;
        timer.OnTimeUp += HandleTimeUp;
        live.OnLivesRemoved += HandleLivesRemoved;
    }

    private void Start()
    {
        progressHandler.TotalCount = wordPicker.FillUpList() + sentenceHandler.FillUpList();
        TryUpdateScrambleExercise();
    }


    public void TryUpdateScrambleExercise()
    {
        current = GetRandomType();
        switch (current)
        {
            case ScrambleType.Letter:
                sentenceHandler.gameObject.SetActive(false);
                wordPicker.PickWord();
                break;
            case ScrambleType.Sentence:
                wordPicker.gameObject.SetActive(false);
                sentenceHandler.PickSentence();
                break;
            case ScrambleType.None:
            default:
                scoreHandler.AddBonusPerRemainingAttempt(live.LivesCount);
                OnStageFinished?.Invoke();
                break;
        }
    }

    private void HandlePicked(int initialCount, int currentCount)
    {
        timer.Restart();
    }

    private void HandleTimeUp()
    {
        if (current == ScrambleType.Letter)
            wordHandler.SubmitWord();
        else
            sentenceHandler.SubmitSentence();
    }

    private void HandleExerciseSubmitted(bool correct)
    {
        timer.Paused = true;
        if (!correct)
            live.RemoveLive();
        else
        {
            Streak++;
            scoreHandler.AddScore();
        }
    }

    private void HandleLivesRemoved()
    {
        if (live.LivesCount > 0) return;
        OnStageFinished?.Invoke();
    }

    private ScrambleType GetRandomType()
    {
        if (wordPicker.CanPickWord && sentenceHandler.CanPickSentence)
        {
            int random = Random.Range(1, Enum.GetNames(typeof(ScrambleType)).Length);
            //Debug.Log($"Can pick both words and sentences {random}");
            return (ScrambleType)random;
        }
        if (wordPicker.CanPickWord)
        {
            //Debug.Log("Can pick words only");
            return ScrambleType.Letter;
        }
        if (sentenceHandler.CanPickSentence)
        {
            //Debug.Log("Can pick sentences only");
            return ScrambleType.Sentence;
        } 
        //Debug.Log("Can pick nothing");
        return ScrambleType.None;
    }
}
