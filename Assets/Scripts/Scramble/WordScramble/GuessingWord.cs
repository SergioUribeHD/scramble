using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GuessingWord
{
    [field: SerializeField] public string Word { get; private set; } = string.Empty;
    [field: SerializeField] public VarReplacer Sentence { get; private set; } = new VarReplacer("_________ attacks occur via targeted emails and websites hackeronis");

    public string GetFullSentence(Color correctColor) => Sentence.GetReplacedText(RichText.GetColoredText(Word, correctColor));
}
