using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequiredWordPicker : MonoBehaviour
{
    [SerializeField] private WordHandler wordHandler = null;
    [SerializeField] private UnorderedRack unorderedRack = null;
    [SerializeField] private OrderedRack orderedRack = null;
    [SerializeField] private Text sentenceText = null;
    [SerializeField] private Color correctWordColor = Color.green;
    [SerializeField] private Color wrongWordColor = Color.red;
    [SerializeField] private List<GuessingWord> words = new List<GuessingWord>();

    private int initialWordsCount;

    private List<GuessingWord> remainingWords = new List<GuessingWord>();
    public event Action OnWordsCompleted = null;
    public event Action<int, int> OnWordPicked = null;

    public bool CanPickWord => remainingWords.Count > 0;

    private GuessingWord currentWord;

    private void OnEnable()
    {
        wordHandler.OnWordSubmitted += HandleWordSubmitted;
    }

    public int FillUpList()
    {
        remainingWords.AddRange(words);
        initialWordsCount = remainingWords.Count;
        return initialWordsCount;
    }

    private void OnDisable()
    {
        wordHandler.OnWordSubmitted -= HandleWordSubmitted;
    }

    private void HandleWordSubmitted(bool correct)
    {
        Color color = correct ? correctWordColor : wrongWordColor;
        sentenceText.text = currentWord.GetFullSentence(color);
    }

    public bool TryPickWord()
    {
        if (CanPickWord)
        {
            PickWord();
            return true;
        }
        OnWordsCompleted?.Invoke();
        return false;
    }

    public void PickWord()
    {
        gameObject.SetActive(true);
        currentWord = remainingWords.GetRandomItem(true);
        wordHandler.RequiredWord = currentWord.Word;
        sentenceText.text = currentWord.Sentence.OriginalText;
        unorderedRack.Set(wordHandler.RequiredWord);
        orderedRack.Set(wordHandler.RequiredWord.Length);
        OnWordPicked?.Invoke(initialWordsCount - remainingWords.Count, initialWordsCount);
    }

}
