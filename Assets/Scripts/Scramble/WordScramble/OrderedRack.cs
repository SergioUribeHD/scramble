using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderedRack : MonoBehaviour
{
    [SerializeField] private LetterContainer letterContainerPrefab = null;

    public event Action OnWordUpdated = null;

    public string Word
    {
        get
        {
            string word = string.Empty;
            foreach (var container in LetterContainers)
                word += container.Value;
            return word;
        }
    }

    public List<LetterContainer> LetterContainers { get; } = new List<LetterContainer>();

    private void OnEnable()
    {
        Letter.OnClick += HandleLetterClicked;
    }

    private void OnDisable()
    {
        Letter.OnClick -= HandleLetterClicked;
    }

    public void Set(int count)
    {
        LetterContainers.CheckInstancesNeeded(letterContainerPrefab, transform, count);
        for (int i = 0; i < count; i++)
            LetterContainers[i].gameObject.SetActive(true);
        for (int i = count; i < LetterContainers.Count; i++)
            LetterContainers[i].gameObject.SetActive(false);
    }

    private void HandleLetterClicked(Letter letter)
    {
        if (letter.Container != null)
            letter.Restart();
        else
        {
            foreach (var letterContainer in LetterContainers)
            {
                if (letterContainer.transform.childCount > 0) continue;
                letter.Container = letterContainer;
                break;
            }
        }
        OnWordUpdated?.Invoke();
    }


}
