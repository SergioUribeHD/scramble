using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RecursiveInstantiator
{
    public static void CheckInstancesNeeded<T>(this List<T> list, T prefab, Transform parent, int count) where T : Object
    {
        int diff = count - list.Count;
        if (diff <= 0) return;
        for (int i = 0; i < diff; i++)
            list.Add(GameObject.Instantiate(prefab, parent));
    }
}
