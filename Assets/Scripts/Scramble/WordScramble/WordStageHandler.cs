using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WordStageHandler : MonoBehaviour
{
    [SerializeField] private Timer timer = null;
    [SerializeField] private ScoreHandler scoreHandler = null;
    [SerializeField] private WordHandler wordHandler = null;
    [SerializeField] private RequiredWordPicker wordPicker = null;

    public event Action<int, int> OnStageFinished = null;

    private int streak;
    private int wordsCount;

    private void Awake()
    {
        wordHandler.OnWordSubmitted += HandleWordSubmitted;
        wordPicker.OnWordPicked += HandleWordPicked;
        timer.OnTimeUp += HandleTimeUp;
        wordPicker.OnWordsCompleted += HandleWordsCompleted;
    }

    private void OnDestroy()
    {
        wordHandler.OnWordSubmitted -= HandleWordSubmitted;
        wordPicker.OnWordPicked -= HandleWordPicked;
        timer.OnTimeUp -= HandleTimeUp;
        wordPicker.OnWordsCompleted -= HandleWordsCompleted;
    }

    private void HandleWordsCompleted()
    {
        OnStageFinished?.Invoke(streak, wordsCount);
    }

    private void HandleWordPicked(int initialCount, int currentCount)
    {
        wordsCount = initialCount;
        timer.Restart();
    }

    private void HandleTimeUp()
    {
        wordHandler.SubmitWord();
    }


    private void HandleWordSubmitted(bool correct)
    {
        timer.Paused = true;
        if (!correct) return;
        streak++;
        scoreHandler.AddScore();
    }
}
