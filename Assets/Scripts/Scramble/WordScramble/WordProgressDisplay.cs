using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordProgressDisplay : MonoBehaviour
{
    [SerializeField] private RequiredWordPicker picker = null;
    [SerializeField] private Text text = null;
    [SerializeField] private VarReplacer replacer = null;

    private void Awake()
    {
        picker.OnWordPicked += HandleWordPicked;
    }

    private void OnDestroy()
    {
        picker.OnWordPicked -= HandleWordPicked;
    }

    private void HandleWordPicked(int initialCount, int currentCount)
    {
        text.text = replacer.GetReplacedText(new object[] { initialCount, currentCount });
    }
}
