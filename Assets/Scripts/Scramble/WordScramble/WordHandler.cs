using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordHandler : MonoBehaviour
{
    [SerializeField] private OrderedRack orderedRack = null;
    [SerializeField] private RequiredWordPicker picker = null;
    [SerializeField] private GameObject blockPanel = null;
    [SerializeField] private Button submitBtn = null;
    [SerializeField] private Button nextBtn = null;

    public event Action<bool> OnWordSubmitted;

    public string RequiredWord { get; set; } = string.Empty;

    private void OnEnable()
    {
        submitBtn.onClick.AddListener(SubmitWord);
        picker.OnWordPicked += HandleWordPicked;
        orderedRack.OnWordUpdated += HandleWordUpdated;
    }

    private void OnDisable()
    {
        submitBtn.onClick.RemoveListener(SubmitWord);
        picker.OnWordPicked -= HandleWordPicked;
        orderedRack.OnWordUpdated -= HandleWordUpdated;
    }

    private void HandleWordPicked(int currentWord, int wordCount)
    {
        HandleWordUpdated();
        blockPanel.SetActive(false);
    }

    private void HandleWordUpdated()
    {
        //Debug.Log($"Word length: {orderedRack.Word.Length}. Required Word length: {RequiredWord.Length}.");
        submitBtn.interactable = orderedRack.Word.Length == RequiredWord.Length;
    }

    public void SubmitWord()
    {
        OnWordSubmitted?.Invoke(orderedRack.Word.ToUpper().Equals(RequiredWord.ToUpper()));
        submitBtn.gameObject.SetActive(false);
        nextBtn.gameObject.SetActive(true);
        blockPanel.SetActive(true);
    }
}
