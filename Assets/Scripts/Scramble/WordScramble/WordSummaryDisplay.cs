using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordSummaryDisplay : MonoBehaviour
{
    [SerializeField] GameObject panel = null;
    [SerializeField] private WordStageHandler stageHandler = null;
    [SerializeField] private Text streakText = null;
    [SerializeField] private VarReplacer streakReplacer = null;

    private void Awake()
    {
        stageHandler.OnStageFinished += HandleStageFinished;
    }

    private void Start()
    {
        panel.SetActive(false);
    }

    private void OnDestroy()
    {
        stageHandler.OnStageFinished -= HandleStageFinished;
    }

    private void HandleStageFinished(int streak, int wordsCount)
    {
        panel.SetActive(true);
        streakText.text = streakReplacer.GetReplacedText(new object[] { streak, wordsCount });
    }
}
