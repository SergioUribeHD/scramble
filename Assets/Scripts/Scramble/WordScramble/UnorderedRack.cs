using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnorderedRack : MonoBehaviour
{
    [SerializeField] private Letter letterPrefab = null;
    [SerializeField] private ContentSizeFitter fitter = null;
    [SerializeField] private HorizontalLayoutGroup layout = null;

    private List<Letter> letters = new List<Letter>();

    public void Set(string word)
    {
        StartCoroutine(CallWordSetup(word));
    }

    private IEnumerator CallWordSetup(string word)
    {
        fitter.enabled = layout.enabled = true;
        word = word.ToUpper();
        letters.CheckInstancesNeeded(letterPrefab, transform, word.Length);
        var letterChars = new List<char>(word);
        for (int i = 0; i < word.Length; i++)
            letters[i].Set(letterChars.GetRandomItem(true), transform);
        for (int i = word.Length; i < letters.Count; i++)
            letters[i].SetActive(transform, false);
        yield return new WaitForEndOfFrame();
        fitter.enabled = layout.enabled = false;
    }
}
