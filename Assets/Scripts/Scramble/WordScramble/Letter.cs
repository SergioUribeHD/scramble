using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Letter : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private Text text = null;
    [SerializeField] private Image image = null;
    [SerializeField] private bool disableImgInContainer = false;

    public static event Action<Letter> OnClick = null;

    private char value;
    public LetterContainer Container
    {
        get => container;
        set
        {
            if (value == null && container != null)
                container.Value = string.Empty;
            container = value;
            if (container == null) return;
            image.enabled = !disableImgInContainer;
            transform.position = container.transform.position;
            transform.SetParent(container.transform);
            transform.localScale = Vector3.one;
            container.Value = this.value.ToString();
        }
    }


    private Vector3 initialPos;
    private Transform initialParent;
    private LetterContainer container;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Container == null)
            initialPos = transform.position;
        OnClick?.Invoke(this);
    }

    public void Set(char letter, Transform initialParent)
    {
        SetActive(initialParent, true);
        text.text = letter.ToString();
        value = letter;
    }

    public void SetActive(Transform initialParent, bool active)
    {
        Container = null;
        this.initialParent = initialParent;
        transform.SetParent(initialParent);
        transform.localScale = Vector3.one;
        gameObject.SetActive(active);
        image.enabled = true;
    }

    public void Restart()
    {
        if (Container == null) return;
        Container = null;
        transform.SetParent(initialParent);
        transform.localScale = Vector3.one;
        transform.position = initialPos;
        image.enabled = true;
    }

    private void OnDestroy()
    {
        Container = null;
    }


}
