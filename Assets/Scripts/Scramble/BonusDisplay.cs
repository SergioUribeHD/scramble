using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusDisplay : MonoBehaviour
{
    [SerializeField] private Text text = null;
    [SerializeField] private ScoreHandler scoreHandler = null;

    private void Awake()
    {
        scoreHandler.OnUpdated += UpdateBonusText;
    }

    private void Start()
    {
        text.text = scoreHandler.TotalTimeBonus > 0 ? $"+{scoreHandler.TotalTimeBonus}" : "0";
        
    }

    private void UpdateBonusText()
    {
        text.text = scoreHandler.TotalTimeBonus > 0 ? $"+{scoreHandler.TotalTimeBonus}" : "0";
    }

}
