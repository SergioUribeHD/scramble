﻿using System.Collections.Generic;

public static class ListAdditionalMethods
{

    /// <summary>
    /// Returns a random item
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="remove">If true, removes the returned item from the list.</param>
    /// <returns></returns>
    public static T GetRandomItem<T>(this List<T> list, bool remove)
    {
        T item = list[UnityEngine.Random.Range(0, list.Count)];
        if (remove)
            list.Remove(item);
        return item;
    }

    /// <summary>
    /// Returns a random item and removes it from the list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <returns></returns>
    public static T GetRandomItem<T>(this List<T> list) => list.GetRandomItem(false);

    public static T GetLastItem<T>(this List<T> list) => list[list.Count - 1];

    public static T RemoveLastItem<T>(this List<T> list)
    {
        var item = list.GetLastItem();
        list.Remove(item);
        return item;
    }

    public static bool TryFind<T>(this List<T> list, System.Predicate<T> match, out T item)
    {
        item = list.Find(match);
        return item != null;
    }

    /// <summary>
    /// Gets a list of items in a random order based on your current list. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// The count that you want the list with the items spread randomly to have. If your list has less items than count has, some items will repeat.
    /// <param name="count"></param>
    /// <returns></returns>
    public static List<T> GetListOfRandomItems<T>(this List<T> list, int count)
    {
        if (list.Count == 0) return new List<T>();
        var collection = new List<T>(list);
        var listRandomItems = new List<T>();
        for (int i = 0; i < count; i++)
        {
            if (collection.Count == 0) collection.AddRange(list);
            var randomItem = collection[UnityEngine.Random.Range(0, collection.Count)];
            listRandomItems.Add(randomItem);
            collection.Remove(randomItem);
        }
        return listRandomItems;
    }
}
