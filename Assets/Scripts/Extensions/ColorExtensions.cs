using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorExtensions
{
    /// <summary>
    /// Returns a text inside a color tag to use in text components in unity.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="textInsideTag"></param>
    /// <returns></returns>
    public static string ToRichText(this Color color, string textInsideTag) => $"<color=#{color.ToHex()}>{textInsideTag}</color>";

    public static string ToHex(this Color color)
    {
        int r = Get255Value(color.r);
        int g = Get255Value(color.g);
        int b = Get255Value(color.b);
        return $"{r.ToHex(2)}{g.ToHex(2)}{b.ToHex(2)}";
    }

    private static int Get255Value(float normalizedValue) => (int)(normalizedValue * 255);
}
