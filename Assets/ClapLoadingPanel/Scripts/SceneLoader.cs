﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ClapLoading
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        [SerializeField] private LoadingPanel loadingPanel = null;

        public int CurrentSceneIndex => SceneManager.GetActiveScene().buildIndex;

        public string CurrentSceneName => SceneManager.GetActiveScene().name;

        public void LoadFirstScene() => LoadScene(0);

        public void LoadNextScene() => LoadScene(CurrentSceneIndex + 1);

        public void LoadPreviousScene() => LoadScene(CurrentSceneIndex - 1);

        public void ReloadScene() => LoadScene(CurrentSceneIndex);

        public void LoadScene(int sceneIndex) => StartCoroutine(CallLoadScene(sceneIndex));

        public void LoadScene(string sceneName) => StartCoroutine(CallLoadScene(sceneName));

        private void Start()
        {
            if (!LoadingPanel.Loaded || loadingPanel == null) return;
            loadingPanel.FadeOut();
        }


        #region Coroutines

        IEnumerator CallLoadScene(string sceneName)
        {
            if (loadingPanel == null)
                SceneManager.LoadScene(sceneName);
            else
                yield return CallAsyncSceneLoad(sceneName);
        }

        IEnumerator CallLoadScene(int sceneIndex)
        {
            var scenePath = SceneUtility.GetScenePathByBuildIndex(sceneIndex);
            yield return CallLoadScene(GetSceneNameByPath(scenePath));
        }

        IEnumerator CallAsyncSceneLoad(string sceneName)
        {
            yield return loadingPanel.CallFadeIn();
            AsyncOperation asyncSceneLoad = SceneManager.LoadSceneAsync(sceneName);
            asyncSceneLoad.allowSceneActivation = false;
            while (asyncSceneLoad.progress < 0.9f || !loadingPanel.IsLoopAnimationFinished)
                    yield return null;
            asyncSceneLoad.allowSceneActivation = LoadingPanel.Loaded = true;
        }

        #endregion

        #region Static Methods
        public static string GetSceneNameByPath(string path)
        {
            var pathRoutes = path.Split('/');
            var sceneNameFormat = pathRoutes[pathRoutes.Length - 1];
            var format = ".unity";
            return sceneNameFormat.Remove(sceneNameFormat.Length - format.Length);
        }
        #endregion

        public void Quit() => Application.Quit();

    }
}
